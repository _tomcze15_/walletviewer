import { ReactComponent as WalletLogo } from 'assets/icons/wallet-logo.svg';
import { RiDashboardFill } from 'react-icons/ri';
import {
  FaRegChartBar,
  FaWallet,
  FaSun,
  FaMoon,
} from 'react-icons/fa';
import { IoSettingsSharp, IoCheckmarkSharp } from 'react-icons/io5';
import { MdDoubleArrow } from 'react-icons/md';

export {
  WalletLogo,
  RiDashboardFill as Dashboard,
  MdDoubleArrow as Arrows,
  IoSettingsSharp as Settings,
  IoCheckmarkSharp as Checkmark,
  FaRegChartBar as Charts,
  FaWallet as Wallet,
  FaSun as Sun,
  FaMoon as Moon,
};
